
#include "chip.h"
#include "timer.h"
#include "lip.h"
#include "examples/udp_cntr/lip_sample_udp_cntr.h"
#include "examples/phy_loop/lip_sample_phy_loop.h"
#include "lprintf.h"

//#define LOOPBACK_TST

#define BLINK_PIN   LPC_PIN_P2_5_GPIO5_5


#define UDP_PORT  11115

static const uint8_t mac[LIP_MAC_ADDR_SIZE] = {0x00, 0x05, 0xF7, 0xFF, 0xFF, 0xFE };
static const uint8_t ip[LIP_IPV4_ADDR_SIZE] = {192,168,12,251};
static const uint8_t mask[LIP_IPV4_ADDR_SIZE] = {255,255,255,0 };
static const uint8_t gate[LIP_IPV4_ADDR_SIZE] = {192,168,12,1};
static const int     use_dhcp = 0;


int main(void)  {
    clock_init();

    LPC_PIN_CONFIG(BLINK_PIN, 0, LPC_PIN_PULLUP);
    LPC_PIN_DIR_OUT(BLINK_PIN);





    LPC_USART3->LCR = UART_LCR_PARITY_EN | 3;
    LPC_USART3->IER = 0;


    unsigned dl = LPC_SYSCLK / (16 * 115200);
    unsigned fract_div = 0;
    unsigned fract_mul = 1;

    LPC_USART3->LCR |= UART_LCR_DLAB_EN;
    LPC_USART3->DLL = dl & 0xFF;
    LPC_USART3->DLM = (dl >> 8) & 0xFF;

    LPC_USART3->FDR =fract_div  | (fract_mul << 4);

    LPC_USART3->LCR &= ~UART_LCR_DLAB_EN;
    LPC_USART3->FCR = UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS;
    LPC_USART3->FCR = UART_FCR_FIFO_EN;

    LPC_PIN_CONFIG(LPC_PIN_P2_3_U3_TXD, 0, LPC_PIN_FAST | LPC_PIN_NOPULL);

    lprintf("test porgram started!\n");

#ifdef LOOPBACK_TST
    t_lip_sample_phy_loop_stat stat;
    memset(&stat, 0, sizeof(stat));
    uint32_t prev_err  = 0;
    lip_sample_phy_loop_init();
#else
    lip_sample_udp_cntr_init(mac, use_dhcp ? NULL : ip, mask, gate, UDP_PORT);
#endif

    t_timer tmr;
    timer_set(&tmr, CLOCK_CONF_SECOND);
    LPC_PIN_OUT(BLINK_PIN, 1);
    for (;;) {
#ifdef LOOPBACK_TST
        int err = lip_sample_phy_loop_pull(&stat);
        if (!err) {
            if (timer_expired(&tmr)) {
                lprintf("packets = %6d, errors = %6d", stat.rcv_packts, stat.rcv_errs);
                if (prev_err != stat.rcv_errs) {
                    lprintf(" !! last err : exp = 0x%08X, rcv = 0x%08X",
                            stat.last_err.exp_wrd, stat.last_err.rcv_wrd);
                }
                lprintf("\n");
            }
        } else {
            lprintf("Критическая ошибка инициализации (%d)! \n", err);
        }
#else
        lip_sample_udp_cntr_pull();

#endif

        if (timer_expired(&tmr) && (lip_state()==e_LIP_STATE_CONFIGURED)) {
            LPC_PIN_TOGGLE(BLINK_PIN);
            timer_reset(&tmr);
        }
    }
}
