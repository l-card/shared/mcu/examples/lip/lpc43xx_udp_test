/*
 * lip_port_conf.h
 *  Файл с настройкаси порта TCP/IP стека LIP
 *  Created on: 03.02.2013
 *      Author: borisov
 */

#ifndef LIP_PORT_CONF_H_
#define LIP_PORT_CONF_H_

/** Используемая конфигурация пина для линии MDC */
#ifdef BOARD_MCB43000
    #define LIP_PHY_PIN_MDC LPC_PIN_PC_1_ENET_MDC
#else
    #define LIP_PHY_PIN_MDC LPC_PIN_P7_7_ENET_MDC
#endif

/** Макрос для задания расположения буферов приема и передачи пакетов */
#define LIP_PORT_BUF_MEM(var)    __attribute__ ((section (".eth_ram"))) var
/** Макрос для задания расположения дескрипторов DMA для Ethernet*/
#define LIP_PORT_DESCR_MEM(var) __attribute__ ((section (".eth_ram"))) var

#if !defined BOARD_MCB43000 && !defined BOARD_E502
    /** Настройка клоков Phy через таймер */
    #define LIP_PORT_PHY_CLK_TIMER
#endif

#endif /* UIP_PORT_CONF_H_ */
